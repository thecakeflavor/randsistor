**The Randsistor... what can it do, what can it really do?**
This is Randsistor, a randomizer mod for Transistor (2014) by Supergiant Games.

So far I've gotten a sort of randomization to work on my own Transistor installation on my own computer.

I'll have to make some sort of installer to add the mod to other people's games.

Planned features:

  * set seed mode

  * randomize entrances

    * all entrances
	
    * safe entrances
	
    * safe entrances, boss ending
	
  * randomize starter functions

    * no checks
	
    * make sure it's beatable with glitch algorithm

    * make sure it's beatable without glitch algorithm

  * randomize levelup functions
	
    * single option (harder)
		
    * default options
		
    * more options (easier)
		
  * randomize upgrades
	
    * single option (harder)
		
    * default options
		
    * more options (easier)
		 
  * randomize limiters
	
    * single option (harder)
		
    * default options
		
    * more options (easier)